" Scheme setup
set background=dark
hi! clear

if exists("syntax_on")
	syntax reset
endif

let colors_name="noctu-mod"

" Vim UI
"hi Cursor              ctermfg=7     ctermbg=1
hi Cursor              ctermfg=7     ctermbg=0     cterm=bold
hi CursorLine                        ctermbg=0     cterm=NONE
"hi MatchParen          ctermfg=7     ctermbg=NONE  cterm=underline
hi MatchParen          ctermfg=0     ctermbg=3     cterm=bold
hi Pmenu               ctermfg=0     ctermbg=0     cterm=bold
hi PmenuThumb                        ctermbg=7
hi PmenuSBar                         ctermbg=0     cterm=bold
hi PmenuSel            ctermfg=0     ctermbg=4
hi ColorColumn                       ctermbg=0
hi SpellBad            ctermfg=1     ctermbg=NONE  cterm=bold
hi SpellCap            ctermfg=6     ctermbg=NONE  cterm=bold
hi SpellRare           ctermfg=3     ctermbg=NONE  cterm=bold
hi SpellLocal          ctermfg=5     ctermbg=NONE  cterm=bold
hi NonText             ctermfg=0                   cterm=bold
hi LineNr              ctermfg=0     ctermbg=NONE  cterm=bold
hi CursorLineNr        ctermfg=3     ctermbg=0     cterm=bold
"hi Visual              ctermfg=0     ctermbg=4     cterm=bold
hi Visual              ctermfg=NONE  ctermbg=NONE  cterm=reverse,bold
"hi IncSearch           ctermfg=0     ctermbg=5     cterm=bold
hi IncSearch           ctermfg=3     ctermbg=NONE  cterm=reverse,bold
"hi Search              ctermfg=0     ctermbg=2     cterm=bold
hi Search              ctermfg=2     ctermbg=NONE  cterm=reverse,bold
hi StatusLine          ctermfg=7     ctermbg=0     cterm=bold
hi StatusLineNC        ctermfg=0     ctermbg=0     cterm=bold
hi VertSplit           ctermfg=0     ctermbg=0     cterm=NONE
hi TabLine             ctermfg=0     ctermbg=0     cterm=bold
hi TabLineSel          ctermfg=7     ctermbg=0
hi Folded              ctermfg=3     ctermbg=0
hi Directory           ctermfg=4                   cterm=bold
hi Title               ctermfg=3                   cterm=bold
"hi ErrorMsg            ctermfg=0     ctermbg=1     cterm=bold
hi ErrorMsg            ctermfg=1     ctermbg=NONE  cterm=NONE
hi DiffAdd             ctermfg=2     ctermbg=NONE  cterm=bold
hi DiffChange          ctermfg=3     ctermbg=NONE  cterm=bold
hi DiffDelete          ctermfg=1     ctermbg=NONE  cterm=bold
hi DiffText            ctermfg=3     ctermbg=NONE  cterm=bold
hi diffFile            ctermfg=4     ctermbg=NONE  cterm=bold
hi diffLine            ctermfg=6     ctermbg=NONE  cterm=bold
hi gitHash             ctermfg=5     ctermbg=NONE  cterm=bold
hi User1               ctermfg=7     ctermbg=5     cterm=bold
hi User2               ctermfg=7     ctermbg=0     cterm=bold
hi User3               ctermfg=7     ctermbg=3     cterm=bold
hi User4               ctermfg=7     ctermbg=0     cterm=bold
hi User5               ctermfg=7     ctermbg=5     cterm=bold
hi User6               ctermfg=7     ctermbg=6     cterm=bold
hi User7               ctermfg=7     ctermbg=4     cterm=bold
hi User8               ctermfg=7     ctermbg=3     cterm=bold
hi User9               ctermfg=7     ctermbg=0     cterm=bold
hi! link diffAdded DiffAdd
hi! link diffChanged DiffChange
hi! link diffRemoved DiffDelete
hi! link CursorColumn  CursorLine
hi! link SignColumn    LineNr
hi! link WildMenu      Visual
hi! link FoldColumn    SignColumn
hi! link WarningMsg    ErrorMsg
hi! link MoreMsg       Title
hi! link Question      MoreMsg
hi! link ModeMsg       MoreMsg
hi! link TabLineFill   StatusLineNC
hi! link SpecialKey    NonText

"
" Generic syntax
"

"hi Delimiter       ctermfg=7   cterm=NONE
hi Delimiter       ctermfg=3   cterm=bold
hi Comment         ctermfg=0   cterm=bold
hi Underlined      ctermfg=4   cterm=underline
hi Type            ctermfg=4
hi String          ctermfg=4   cterm=bold
hi Keyword         ctermfg=2
hi Todo            ctermfg=7   ctermbg=NONE     cterm=bold
hi Function        ctermfg=4
hi Identifier      ctermfg=2   cterm=NONE
hi Statement       ctermfg=2   cterm=bold
hi Constant        ctermfg=5   cterm=bold
hi Number          ctermfg=4   cterm=bold
hi Boolean         ctermfg=4
"hi Special         ctermfg=5   cterm=bold
hi Special         ctermfg=3
hi Ignore          ctermfg=0
hi! link Operator  Delimiter
hi! link PreProc   Delimiter
hi! link Error     ErrorMsg

" HTML
hi htmlTagName              ctermfg=2
hi htmlTag                  ctermfg=2
hi htmlArg                  ctermfg=10
hi htmlH1                   cterm=bold
hi htmlBold                 cterm=bold
hi htmlItalic               cterm=underline
hi htmlUnderline            cterm=underline
hi htmlBoldItalic           cterm=bold,underline
hi htmlBoldUnderline        cterm=bold,underline
hi htmlUnderlineItalic      cterm=underline
hi htmlBoldUnderlineItalic  cterm=bold,underline
hi! link htmlLink           Underlined
hi! link htmlEndTag         htmlTag

" XML
hi xmlTagName       ctermfg=4
hi xmlTag           ctermfg=4   cterm=bold
hi! link xmlString  xmlTagName
hi! link xmlAttrib  xmlTag
hi! link xmlEndTag  xmlTag
hi! link xmlEqual   xmlTag

" JavaScript
hi! link javaScript        Normal
hi! link javaScriptBraces  Delimiter

" PHP
hi phpSpecialFunction    ctermfg=5
hi phpIdentifier         ctermfg=3      cterm=bold
hi! link phpVarSelector  phpIdentifier
hi! link phpHereDoc      String
hi! link phpDefine       Statement

" Markdown
hi! link markdownHeadingRule        NonText
hi! link markdownHeadingDelimiter   markdownHeadingRule
hi! link markdownLinkDelimiter      Delimiter
hi! link markdownURLDelimiter       Delimiter
hi! link markdownCodeDelimiter      NonText
hi! link markdownLinkTextDelimiter  markdownLinkDelimiter
hi! link markdownUrl                markdownLinkText
hi! link markdownAutomaticLink      markdownLinkText
hi! link markdownCodeBlock          String
hi markdownCode                     cterm=bold
hi markdownBold                     cterm=bold
hi markdownItalic                   cterm=underline

" Ruby
hi! link rubyDefine                 Statement
hi! link rubyLocalVariableOrMethod  Identifier
hi! link rubyConstant               Constant
hi! link rubyInstanceVariable       Number
hi! link rubyStringDelimiter        rubyString

" Git
hi gitCommitBranch               ctermfg=3
hi gitCommitSelectedType         ctermfg=2    cterm=bold
hi gitCommitSelectedFile         ctermfg=2
hi gitCommitUnmergedType         ctermfg=1    cterm=bold
hi gitCommitUnmergedFile         ctermfg=1
hi! link gitCommitFile           Directory
hi! link gitCommitUntrackedFile  gitCommitUnmergedFile
hi! link gitCommitDiscardedType  gitCommitUnmergedType
hi! link gitCommitDiscardedFile  gitCommitUnmergedFile

" Vim
hi! link vimSetSep    Delimiter
hi! link vimContinue  Delimiter
hi! link vimHiAttrib  Constant

" LESS
hi lessVariable             ctermfg=3   cterm=bold
hi! link lessVariableValue  Normal

" NERDTree
hi! link NERDTreeHelp      Comment
hi! link NERDTreeExecFile  String

" Vimwiki
hi! link VimwikiHeaderChar  markdownHeadingDelimiter
hi! link VimwikiList        markdownListMarker
hi! link VimwikiCode        markdownCode
hi! link VimwikiCodeChar    markdownCodeDelimiter

" Help
hi! link helpExample         String
hi! link helpHeadline        Title
hi! link helpSectionDelim    Comment
hi! link helpHyperTextEntry  Statement
hi! link helpHyperTextJump   Underlined
hi! link helpURL             Underlined

" CtrlP
hi! link CtrlPMatch    String
hi! link CtrlPLinePre  Comment

" Mustache
hi mustacheSection           ctermfg=6  cterm=bold
hi mustacheMarker            ctermfg=6
hi mustacheVariable          ctermfg=6  cterm=bold
hi mustacheVariableUnescape  ctermfg=1  cterm=bold
hi mustachePartial           ctermfg=5  cterm=bold

" Shell
"hi shDerefSimple     ctermfg=3      cterm=bold
hi shDerefSimple     ctermfg=2      cterm=NONE
hi! link shDerefVar  shDerefSimple

" Syntastic
hi SyntasticWarningSign  ctermfg=3   ctermbg=NONE
hi SyntasticErrorSign    ctermfg=1   ctermbg=NONE

" Netrw
hi netrwExe       ctermfg=1  cterm=bold
hi netrwClassify  ctermfg=0  cterm=bold


" own additions

hi texBoldStyle cterm=NONE ctermfg=NONE ctermbg=NONE gui=NONE
hi texItalStyle cterm=NONE ctermfg=NONE ctermbg=NONE gui=NONE
hi texBoldItalStyle cterm=NONE ctermfg=NONE ctermbg=NONE gui=NONE
hi texItalBoldStyle cterm=NONE ctermfg=NONE ctermbg=NONE gui=NONE
