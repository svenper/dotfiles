"set encoding=utf-8
"filetype plugin indent on
syntax on

set spellfile+=~/.local/share/nvim/site/spell/sv.utf-8.add

if has('nvim')
	set shada=
else
	set t_Co=16
	set t_ti= t_te=
endif

colorscheme noctu-mod
"set background=dark
"colorscheme disco
"set background=dark
"colorscheme default
"set background=dark

autocmd BufNewFile,BufRead template set filetype=sh
autocmd BufNewFile,BufRead APKBUILD set filetype=sh
autocmd BufNewFile,BufRead PKGBUILD set filetype=sh
autocmd BufNewFile,BufRead *.ebuild set filetype=sh

autocmd BufNewFile,BufRead *.patch set filetype=diff

autocmd BufNewFile,BufRead *.tex set filetype=tex

autocmd BufNewFile,BufRead *.fd set filetype=tex
autocmd BufNewFile,BufRead *.def set filetype=tex
autocmd BufNewFile,BufRead *.cfg set filetype=tex

autocmd BufNewFile,BufRead *.bbx set filetype=tex
autocmd BufNewFile,BufRead *.lbx set filetype=tex

autocmd BufNewFile,BufRead *.ldf set filetype=tex
autocmd BufNewFile,BufRead *.cbx set filetype=tex

autocmd BufNewFile,BufRead *.pygstyle set filetype=tex

autocmd FileType tex set spell
autocmd FileType tex set spelllang=sv,en

autocmd FileType tex colorscheme noctu-mod
"autocmd FileType tex colorscheme disco

"autocmd BufNewFile,BufRead * set background=dark
"autocmd BufNewFile,BufRead * colorscheme default
"autocmd BufNewFile,BufRead * set background=dark
"autocmd BufNewFile,BufRead * highlight Search cterm=reverse ctermfg=none ctermbg=none

set path+=**
set wildmenu

"set nowrap
set nobackup
set nowritebackup
set noswapfile
let g:netrw_dirhistmax=0
let g:netrw_home='/tmp/vim'

set ignorecase
set smartcase
set incsearch
set showcmd
set ruler
set laststatus=1
set viminfo=
"set expandtab
set noexpandtab
set shiftwidth=8
set tabstop=8
set softtabstop=8
"set autoindent
"set smartindent
"set cindent
set nojoinspaces
set hlsearch
"set relativenumber
set formatoptions+=j

set modeline
set modelines=5

set mouse=

nnoremap <up>    <nop>
nnoremap <down>  <nop>
nnoremap <left>  <nop>
nnoremap <right> <nop>
inoremap <up>    <nop>
inoremap <down>  <nop>
inoremap <left>  <nop>
inoremap <right> <nop>

"hi Constant cterm=bold ctermfg=red
"hi Keyword cterm=bold
"hi Define cterm=bold
"hi String cterm=bold ctermfg=blue
"hi Statement cterm=bold ctermfg=yellow
"hi Identifier cterm=NONE ctermfg=green
"hi Label cterm=bold
"hi Title cterm=bold
"hi Special cterm=bold ctermfg=NONE
"hi PreProc cterm=NONE ctermfg=green
