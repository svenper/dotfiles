[ -d ~/bin ] && echo $PATH | grep --quiet "$HOME/bin" || export PATH=$HOME/bin:$PATH
[ -d /opt/local/bin ] && (echo $PATH | grep --quiet "/opt/local/bin" || export PATH=/opt/local/bin:$PATH)

export PS1=$(tput setaf 7)$(tput bold)'$'$(tput sgr0)' '
export EDITOR=vim
export VISUAL=vim
export PAGER=less
export LESS=-iRXFM
export LESSHISTFILE=/dev/null
# these contain an escaped escape (sic) (^[) that may not be rendered correctly everywhere
export LESS_TERMCAP_mb='[1;35m'
export LESS_TERMCAP_md='[1;35m'
export LESS_TERMCAP_me='[0;37m'
export LESS_TERMCAP_so='[7;37m'
export LESS_TERMCAP_se='[0;37m'
export LESS_TERMCAP_us='[1;36m'
export LESS_TERMCAP_ue='[0;37m'
export GREP_COLORS='ms=01;32:mc=01;32:sl=:cx=01;30:fn=01;30:ln=33:bn=01;33:se=01;36'
#if [ "$0" = "bash" ]; then
#	unset POSIXLY_CORRECT
#else
#	export POSIXLY_CORRECT=1
#fi

command -v dircolors 1>/dev/null 2>/dev/null
if [ $? -eq 0 ]
then	LS_COLORS=$(dircolors -b | sed -E -e '/export/d' -e "s/^LS_COLORS='(.*)';$/\1/g")
	export LS_COLORS
fi

BASH_ENV_FIXED="env -u POSIXLY_CORRECT HISTSIZE=0 HISTFILE=/dev/null bash -l"
alias bash="${BASH_ENV_FIXED}"
alias b="${BASH_ENV_FIXED}"
alias s="sh -l"
alias z="zsh -l"
alias x="cd ~; startx -- vt$(tty | sed -E 's|.*tty([0-9]+)|\1|g')"
alias sudo="sudo "
alias emacs="emacs -nw"
alias grep="grep --color=auto"
alias psed="perl -Mutf8 -CADS -Mfeature=unicode_strings -p"
alias fixul='perl -pe s#'\''\[4m'#'\[36m'\''#g'

command -v pacman 1>/dev/null 2>/dev/null
if [ $? -eq 0 ]
then alias pacman="pacman --color=auto"
fi

ls --version 2>/dev/null | grep --quiet GNU
if [ $? -eq 0 ]
then alias ls="ls -h -N --time-style=long-iso --color=auto"
     alias la="ls -lA"
else alias ls="ls -h -G -T"
fi

diff --color=auto /dev/null /dev/null 1>/dev/null 2>/dev/null
if [ $? -eq 0 ]
then alias diff="diff --color=auto"
fi

command -v mandb-man 1>/dev/null 2>/dev/null
if [ $? -eq 0 ]
then MAN=mandb-man
else MAN=man
fi

pdf() {
    (mupdf "$@" &)
}

pdfgl() {
    (mupdf-gl "$@" &)
}
