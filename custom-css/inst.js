if ( (location.protocol === 'http:') || (location.protocol === 'https:') ) (function() {

    'use strict';

    var customcss = document.createElement('link');
    customcss.rel = 'stylesheet';
    customcss.href = chrome.runtime.getURL('Custom.css');
    document.head.appendChild(customcss);

})();
