source $HOME/.profile

bindkey -e

bindkey "\e[1~" beginning-of-line
bindkey "\e[4~" end-of-line
bindkey "\e[5~" history-beginning-search-backward
bindkey "\e[6~" history-beginning-search-forward
bindkey "\e[3~" delete-char
bindkey "\e[2~" quoted-insert

# for rxvt
bindkey "\e[8~" end-of-line
bindkey "\eOc" forward-word
bindkey "\eOd" backward-word

# for non RH/Debian xterm, can't hurt for RH/Debian xterm
bindkey "\eOH" beginning-of-line
bindkey "\eOF" end-of-line

# for freebsd console
bindkey "\e[H" beginning-of-line
bindkey "\e[F" end-of-line

# manually added
bindkey "\e\e[C"  forward-word
bindkey "\e\e[D"  backward-word

PROMPT="%(#.%F{red}.%F{green})%B%n%b%f%B%F{white}@%f%b%m %F{white}%B%~%b%f %B%(#.%F{red}#.%F{green}$)%f%b "
RPROMPT="%(?.%F{green}.%F{red})%B%?%b%f"
#ZLE_RPROMPT_INDENT=0 # doesn't quite work on kernel vconsole without tmux

PROMPT_EOL_MARK='%K{magenta}%F{green}%%%f%k'

zeh_timer=
zeh_now=
zeh_elapsed=
zeh_elapsed_h=
zeh_elapsed_m=
zeh_elapsed_s=
zeh_elapsed_ms=
zeh_separat_h=
zeh_separat_m=
zeh_separat_s=
zeh_separat_ms=

function preexec() {
	zeh_timer=$(($(date +%s%0N)/1000000.0))
}

function precmd() {
	if [ -n "${zeh_timer}" ]; then
		zeh_now="$(($(date +%s%0N)/1000000.0))"
		zeh_elapsed="$(printf '%d' $((${zeh_now}-${zeh_timer})))"
		zeh_elapsed_ms="$(printf '%d' $((${zeh_elapsed}%1000)))"
		zeh_elapsed_s="$(printf '%.0f' $(((${zeh_elapsed}/1000.0)%60.0)))"
		zeh_elapsed_m="$(printf '%d' $(((${zeh_elapsed}/1000)%3600/60)))"
		zeh_elapsed_h="$(printf '%d' $(((${zeh_elapsed}/1000)/3600)))"
		zeh_separat_ms="ms"
		zeh_separat_s="s"
		zeh_separat_m="m "
		zeh_separat_h="h "
		[ ${zeh_elapsed} -ge 1000 ] && zeh_elapsed_ms="" && zeh_separat_ms=""
		[ ${zeh_elapsed_h} = 0 ] && [ ${zeh_elapsed_m} = 0 ] && [ ${zeh_elapsed} -lt 1000 ] && zeh_elapsed_s=""  && zeh_separat_s=""
		[ ${zeh_elapsed_h} = 0 ] && [ ${zeh_elapsed_m} = 0 ]                                && zeh_elapsed_m=""  && zeh_separat_m=""
		[ ${zeh_elapsed_h} = 0 ]                                                            && zeh_elapsed_h=""  && zeh_separat_h=""
		RPROMPT="%B%F{blue}${zeh_elapsed_h}${zeh_separat_h}${zeh_elapsed_m}${zeh_separat_m}${zeh_elapsed_s}${zeh_separat_s}${zeh_elapsed_ms}${zeh_separat_ms}%f%b %(?.%F{green}.%F{red})%B%?%b%f"
		unset zeh_timer
	fi
}

#REPORTTIME=1200
HISTSIZE=1048575
SAVEHIST=$HISTSIZE
HISTFILE=$HOME/.zhistory

setopt INC_APPEND_HISTORY # seems to break EXTENDED_HISTORY
setopt SHARE_HISTORY # seems to break EXTENDED_HISTORY
setopt HIST_IGNORE_SPACE
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
setopt HIST_REDUCE_BLANKS
# setopt EXTENDED_HISTORY
setopt CORRECT
setopt GLOB_COMPLETE
setopt RM_STAR_SILENT

unsetopt NOMATCH

autoload -U compinit
compinit -d $HOME/.zcompdump

zstyle ':completion:*:descriptions' format '%B%d%b'
